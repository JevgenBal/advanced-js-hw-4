// get list of film
function getFilms() {
fetch('https://ajax.test-danit.com/api/swapi/films')
	.then(response => response.json())
	.then(films => displayFilms(films))
	.catch(error => console.error(error));
}

// show list of film
function displayFilms(films) {
	var filmsContainer = document.getElementById('films');
	films.forEach(function (film) {
		var filmElement = document.createElement('div');
		filmElement.innerHTML = '<h3>Episode ' + film.episodeId + ': ' + film.name + '</h3><p>' + film.openingCrawl + '</p><div id="characters-' + film.episodeId + '"></div>';
		filmsContainer.appendChild(filmElement);
		getCharacters(film.episodeId, film.characters);
	});
}

// get list of actors
function getCharacters(episodeId, characterUrls){
	let charactersContainer = document.getElementById('characters-' + episodeId);
	charactersContainer.innerHTML = '<div class="loader"></div>';

	Promise.all(characterUrls.map(url => fetch(url)
	.then(response => response.json())))
	.then(characters => {
		charactersContainer.innerHTML = '';
		characters.forEach(function(character){
			let characterElement = document.createElement('div');
			characterElement.textContent = character.name;
			charactersContainer.appendChild(characterElement);
		})
	})
	.catch(error => console.error(error))
}

getFilms();